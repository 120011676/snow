package org.qq120011676.snow.base;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.qq120011676.snow.xml.SqlXmlParse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class })
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class BaseJunit {

	private static boolean isFirst = true;

	@BeforeClass
	public static void start() throws Exception {
		if (isFirst) {
			isFirst = false;
			new SqlXmlParse()
					.parse(ClassLoader.getSystemResource("").getPath());
		}

	}

	@AfterClass
	public static void after() {
	}
}
