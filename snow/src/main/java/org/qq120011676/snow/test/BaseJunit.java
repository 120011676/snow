package org.qq120011676.snow.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.qq120011676.snow.properties.ProjectProperties;
import org.qq120011676.snow.util.FileUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class })
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class BaseJunit {

	private static boolean isfirst = true;

	@BeforeClass
	public static void before() throws Exception {
		if (isfirst) {
			isfirst = false;
			ProjectProperties p = new ProjectProperties();
			p.setConfig(FileUtils
					.filePropertiesReader("config"));
			p.setMessages(FileUtils
					.filePropertiesReader("messages"));
		}
	}

	@AfterClass
	public static void after() {
	}
}
