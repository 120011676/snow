package org.qq120011676.snow.xml;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultText;
import org.qq120011676.snow.entity.SqlEntity;
import org.qq120011676.snow.type.SqlType;
import org.qq120011676.snow.util.SqlUtils;
import org.qq120011676.snow.xml.sql.SqlFileFilter;

public class SqlXmlParse extends SqlUtils {

	public void parse(String path) throws DocumentException {
		File[] files = new File(path).listFiles(new SqlFileFilter());
		if (files != null && files.length >= 0) {
			for (File file : files) {
				this.dom4jParseSql(file);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private void dom4jParseSql(File file) throws DocumentException {
		SAXReader saxReader = new SAXReader();
		Document document = saxReader.read(file);
		Element rootElement = document.getRootElement();
		if ("sqls".equals(rootElement.getName())) {
			List elements = rootElement.elements();
			for (Object object : elements) {
				if (object instanceof Element) {
					Element sqlElement = (Element) object;
					if ("sql".equals(sqlElement.getName())) {
						SqlEntity sqlEntity = new SqlEntity();
						List list = sqlElement.content();
						StringBuilder sqlStringBuilder = new StringBuilder();
						Map<String, String> mapParameter = new HashMap<String, String>();
						for (int i = 0; i < list.size(); i++) {
							if (list.get(i) instanceof DefaultText) {
								sqlStringBuilder.append(((DefaultText) list
										.get(i)).getText());
							} else if (list.get(i) instanceof Element) {
								Element elementParameter = (Element) list
										.get(i);
								if ("parameter".equals(elementParameter
										.getName())) {
									String parameterName = elementParameter
											.attributeValue("name");
									if (parameterName == null) {
										throw new RuntimeException(
												"this sql file ["
														+ file.getPath()
														+ "] in parameter tag not exists 'name' attribute.");
									}
									parameterName = parameterName.trim();
									sqlStringBuilder.append("${")
											.append(parameterName).append("}");
									if (mapParameter.containsKey(parameterName)) {
										throw new RuntimeException(
												"this sql file ["
														+ file.getPath()
														+ "] in parameter name '"
														+ parameterName
														+ "' already exists.");
									}
									mapParameter.put(parameterName,
											elementParameter.getTextTrim());
								}
							}
						}
						sqlEntity.setSql(SqlUtils.sqlFormat(sqlStringBuilder
								.toString()));
						sqlEntity.setParameters(mapParameter);
						String type = sqlElement.attributeValue("type");
						if ("sql".equals(type)) {
							sqlEntity.setSqlType(SqlType.SQL);
						} else if ("hql".equals(type)) {
							sqlEntity.setSqlType(SqlType.HQL);
						} else {
							sqlEntity.setSqlType(SqlType.SQL);
						}
						String name = sqlElement.attributeValue("name");
						if (name == null) {
							throw new RuntimeException(
									"this sql file ["
											+ file.getPath()
											+ "] in sql tag not exists 'name' attribute.");
						}
						SqlUtils.add(name.trim(), sqlEntity);
					}
				}
			}
		}
	}
}
