package org.qq120011676.snow.util;

public class ProjectUtils {

	private static String PROJECT_REAL_PATH;

	private static String PROJECT_CLASS_PATH;

	private static String CONTEXT_PATH;

	public void setProjectContextPath(String contextPath) {
		CONTEXT_PATH = contextPath;
	}

	public void setProjectRealPath(String projectRealPath) {
		PROJECT_REAL_PATH = projectRealPath;
	}

	public void setProjectClassPath(String projectClassPath) {
		PROJECT_CLASS_PATH = projectClassPath;
	}

	public static String getProjectRealPath() {
		return PROJECT_REAL_PATH;
	}

	public static String getProjectClassPath() {
		return PROJECT_CLASS_PATH;
	}

	public static String getContextPath() {
		return CONTEXT_PATH;
	}
}
