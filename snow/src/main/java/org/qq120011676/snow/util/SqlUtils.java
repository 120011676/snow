package org.qq120011676.snow.util;

import java.util.HashMap;
import java.util.Map;

import org.qq120011676.snow.entity.SqlEntity;
import org.qq120011676.snow.properties.ProjectProperties;

public class SqlUtils{

	private static Map<String, SqlEntity> SQLS = new HashMap<String, SqlEntity>();

	public static void add(String sqlName, SqlEntity sqlEntity) {
		if (SQLS.containsKey(sqlName)) {
			throw new RuntimeException("this sql name '" + sqlName
					+ "' already exists!");
		}
		SQLS.put(sqlName, sqlEntity);
	}

	public static String getSql(String name) {
		return getSql(name, null);
	}
	
	public static String getSql(String name, Map<String, Object> map) {
		SqlEntity sqlEntity = SQLS.get(name);
		if (sqlEntity == null) {
			throw new RuntimeException("this sql name ‘" + name + "' not exist");
		}
		String sql = sqlEntity.getSql();
		if (map != null && sqlEntity.getParameters() != null) {
			for (String parameterName : map.keySet()) {
				String parameterText = sqlEntity.getParameters().get(
						parameterName);
				sql = ProjectProperties.setMessagesParameter(sql,
						parameterName, parameterText);
			}
		}
		return sqlFormat(sql.replaceAll("\\$\\{[ ]*\\w*[ ]*\\}", ""));
	}

	public static String sqlFormat(String sql) {
		return sql.replaceAll("\n|\t", " ").replaceAll(" +", " ").trim();
	}
}
