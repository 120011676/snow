package org.qq120011676.snow.listener;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.dom4j.DocumentException;
import org.qq120011676.snow.properties.ProjectProperties;
import org.qq120011676.snow.tag.PageTag;
import org.qq120011676.snow.util.FileUtils;
import org.qq120011676.snow.util.FreeMarkerUtils;
import org.qq120011676.snow.util.ProjectUtils;
import org.qq120011676.snow.util.StringUtils;
import org.qq120011676.snow.xml.SqlXmlParse;

public class InitListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		ServletContext servletContext = servletContextEvent.getServletContext();
		ProjectUtils p = new ProjectUtils();
		p.setProjectRealPath(servletContext.getRealPath("/"));
		p.setProjectClassPath(Thread.currentThread().getContextClassLoader()
				.getResource("").getPath());
		p.setProjectContextPath(servletContext.getContextPath());
		this.setFileEncoding(servletContext.getInitParameter("fileEncoding"));
		this.readConfig(servletContext.getInitParameter("configFilePath"));
		this.readMessages(servletContext.getInitParameter("messagesFilePath"));
		try {
			new FreeMarkerUtils().setPath(ProjectUtils.getProjectClassPath());
			this.pageTemplate();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			String sqlfilePath = servletContext.getInitParameter("sqlfilePath");
			sqlfilePath = sqlfilePath == null ? "sqlfile" : sqlfilePath;
			new SqlXmlParse().parse(ProjectUtils.getProjectClassPath()
					+ sqlfilePath);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		servletContext.setAttribute("path", ProjectUtils.getContextPath());
	}

	private void pageTemplate() throws IOException {
		String pageTagFileAddr = ProjectUtils.getProjectClassPath()
				+ PageTag.PAGE_TAG_FILE_PATH;
		if (!FileUtils.isFileOrFolder(pageTagFileAddr)) {
			FileUtils.fileStreamWrite(pageTagFileAddr, this.getClass()
					.getResourceAsStream("/" + PageTag.PAGE_TAG_FILE_PATH));
		}
	}

	private void setFileEncoding(String fileEncoding) {
		if (!StringUtils.isNull(fileEncoding)) {
			System.setProperty("file.encoding", fileEncoding);
		}
	}

	private void readMessages(String messagesFilePath) {
		String messages = null;
		if (!StringUtils.isNull(messagesFilePath)) {
			messages = messagesFilePath;
		} else if (new File(ProjectUtils.getProjectClassPath()
				+ ProjectProperties.DEFAULT_MESSAGES_FILE_NAME
				+ ProjectProperties.DEFAULT_FILE_EXTENSION_NAME).exists()) {
			messages = ProjectProperties.DEFAULT_MESSAGES_FILE_NAME;
		}
		if (!StringUtils.isNull(messages)) {
			new ProjectProperties().setMessages(FileUtils
					.filePropertiesReader(messages));
		}
	}

	private void readConfig(String configFilePath) {
		String config = null;
		if (!StringUtils.isNull(configFilePath)) {
			config = configFilePath;
		} else if (new File(ProjectUtils.getProjectClassPath()
				+ ProjectProperties.DEFAULT_CONFIG_FILE_NAME
				+ ProjectProperties.DEFAULT_FILE_EXTENSION_NAME).exists()) {
			config = ProjectProperties.DEFAULT_CONFIG_FILE_NAME;
		}
		if (!StringUtils.isNull(config)) {
			new ProjectProperties().setConfig(FileUtils
					.filePropertiesReader(config));
		}
	}
}
